Masked Independent Component Analysis (mICA) Toolbox
====================================================

mICA Toolbox is a user-friendly toolbox to perform masked independent component analysis, i.e. ICA within a spatially restricted subregion of the brain. It is based on command line tools from FSL suite to perform ICA and related analyses (e.g. specific data preprocessing, atlas-based mask generation, mICA-based parcellation, dual-regression and direct back reconstruction). Various options provide flexible control of the analysis.

mICA Toolbox is a helpful utility for researchers in the field of neuroimaging. It facillitates the study of intrinsic connectivity of brain (sub-)regions and the reconstruction of their extrinsic connectivity (i.e. connectivity with regions outside of the mask).

The toolbox furthermore offers an easy way to calculate ICA reproducibility over a range of decomposition dimensions, which can be used to overcome the common problem of dimensionality estimation. 


Installation
------------
* Unpack all files in a desired folder
* Install FSL and ensure that FSLDIR environment variable is set (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation)
* Check that you have Python 2.7.6 or higher (Python 3.* not supported yet) installed "python -V". If not, install/update Python. (Only Python 2.* is supported)
* Install required Python modules

  * pip install munkres==1.0.12 matplotlib tk

* Install dc

  * sudo apt install dc

* Start mICA Toolbox using the following command: ``installation_dir/mICA``

