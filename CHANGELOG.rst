Version 1.21
============
* Fixed

  * all_thresh_zstat was incorrectly ordered in the latest Ubuntu
  * Check mixture model convergence before merging thresh_zstat


Version 1.20
============
* Added

  * Experimental dual regression with PALM

* Changed

  * design.fts and design.grp are also copied to dR folder
  * Default number of permutations raised to 5000

* Fixed

  * all_dr_stage3_*_tstat* were not generated if f-test present


Version 1.19
============
* Changed

  * Reproducibility plot

* Fixed

  * Calculation of confidence interval in reproducibility
  * Parsing of arguments in ic_corr.py


Version 1.18
============
* Fixed

  * all_thresh_zstat was incorrectly ordered for mICAS with dim40-49. Higher and lower dimensions did not have this bug.


Version 1.17
============
* Added

  * Number of permutations during dual regression can be changed
  * Exchangeability file for dual regression can be supplied
  * F-test for dual regression.


Version 1.16
============
* Fixed

  * Consistency check during reproducibility analyses


Version 1.15
============
* Changed

  * Compatibility improvement (Linux seq command)

* Fixed

  * Special character in additional melodic flags
  * Multiple custom masks don't get merged


Version 1.14
============
* Added

  * save/load config

* Fixed

  * Minor bugs


Version 1.13
============
* Fixed

  * Pass design file to dual_regression


Version 1.12
============
* Added

  * Reproducibility plots including the 95% confidence intervals
  * Export mean & standard deviation of reproducibility estimates over all split-half repetitions


Version 1.11
============
* Added

  * Generate parcellation based on thresholded IC maps



Version 1.10
============
* Fixed

  * In reproducibility analysis when using automatic dimensionality estimation


Version 1.09
============
* Fixed

  * Multiple custom masks


Version 1.08
============
* Added

  * Custom mask image (as well as background image) can have different resolution as input files if it's resolution passes with the given resampling value.
  * (In ica_inversion) added verbose mode
  * GUI control threshold for Mixture Model

* Fixed

  * When starting ICA with multiple dimensionalities including '0'
  * (In ica_inversion) MixtureModel results will be merged in all_thresh_zstat
