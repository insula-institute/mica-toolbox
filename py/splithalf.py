#!/usr/bin/python

# Copyright (C) 2015 Tawfik Moher Alsady, Florian Beissner
#
# beissner.florian@mh-hannover.de
#
# Developed at
# Somatosensory and Autonomic Therapy Research,
# Institute for Neuroradiology, Hannover Medical School,
# Hannover, Germany
#
# This file is part of the mICA toolbox.
#
# The mICA toolbox is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The mICA toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the mICA toolbox.  If not, see <http://www.gnu.org/licenses/>. 


# This file performs the first step of reproducibility analysis
# random split-half sampling

import sys, math, os, random
	
if len(sys.argv) < 4:
	sys.exit("Usage: splithalf list_filename permutations out_prefix")

# load command-line args
infile = str(sys.argv[1]);
permu = int(sys.argv[2]);
outprefix = str(sys.argv[3]);

# check number of permutations and assign trials list
if permu < 0:
  sys.exit("Error: permutations number is not valid")
elif permu > 0:
  trials = range(0, permu)
else:
  trials = [0] 

# load input list
with open(infile, 'r') as f:
	fileslist = [x.strip('\n') for x in f.readlines()]

# check the input list
if len(fileslist) < 2:
  sys.exit("Error: files list is not valid")

# calculate groupsize = floort(number of files in the input list / 2)
groupsize=math.floor(len(fileslist)/2)

# loop through trials
for trial_num in trials:

	# create output dir
	trial_id = ("%04d" % (trial_num+1))
	output0 = outprefix + "/sample_" + trial_id
	if not os.path.exists(output0):
		os.makedirs(output0)

	# random sampling if not test-retest (when permutation=0)
	rand_allsubj = fileslist[:]
	if permu != 0:
		random.shuffle(rand_allsubj)

	# loop though both split groups
	for groupnum in [0,1]:
		
		# create output dir
		output1 = output0 + "/group" + str(groupnum+1)
		if not os.path.exists(output1):
			os.makedirs(output1)

		# write sampled list to output
		with open(output1 + "_input.txt", 'w') as f:
			for s in rand_allsubj[int((groupnum*groupsize)):int(groupsize+(groupnum*groupsize))]:
				f.write(s + '\n')

